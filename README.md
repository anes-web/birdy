# Birdy

Clone d’un réseau social de type Twitter, mettant en œuvre mes compétences en développement Backend et Frontend.

**Ce projet est encore en cours de développement.<br>**
Le code source sera disponible une fois que le développement sera terminé.<br>
les services fournit par ce projet sont disponibles dans le fichier `all_services.pdf`

Si vous souhaitez tout de meme accéder au code source en cours,<br>
Envoyez moi un mail <a href="mailto:s4jussieu@gmail.com">s4jussieu@gmail.com</a><br>
Je vous envoie le repository tel qu'il est à l'heure actuelle.<br>

_Stay tuned._
